<?php

namespace WordPressApiPopulate\Components;

use WP_Query;
use WP_REST_Request;
use WP_Term_Query;
use WordPressApiPopulate\Helpers;

class Overview
{
    private $ids = [0];

    private $request;

    private $searchParams = [];

    /**
     * Populate overview
     *
     * @param array           $block   Overview block
     * @param WP_REST_Request $request Request
     *
     * @param array Populated overview block
     */
    public function populateOverview(
        array $block,
        WP_REST_Request $request
    ): array {
        $this->request = $request;

        if ($this->request->get_param('search_params')) {
            $this->searchParams = $this->getSearchParams(
                $this->request->get_param('search_params')
            );
        }

        $this->setQueryIds($block['attrs']);

        $query = $this->getQuery($block['attrs']);
        $block['attrs']['initialData'] = $this->getData($query);

        if (array_key_exists('addPagination', $block['attrs'])) {
            if ($block['attrs']['addPagination']) {
                $block['attrs']['initialTotalPosts'] = $query->found_posts;
                $block['attrs']['paginationFriendlyName'] =
                    $this->getPageFilterName();
            }
        }

        if (array_key_exists('innerBlocks', $block)) {
            $block['innerBlocks'] = array_map(
                [$this, 'getFilterData'],
                $block['innerBlocks']
            );
        }

        return $block;
    }

    private function setQueryIds(array $attrs)
    {
        // Arguments object, used to retrieve the initial data
        $args = [
            'post_status' => 'publish',
            'post_type' => array_key_exists('postType', $attrs) ?
                $attrs['postType'] :
                'post',
            'fields' => 'ids',
            'posts_per_page' => -1,
        ];

        if (
            array_key_exists('taxonomy', $attrs) &&
            array_key_exists('terms', $attrs)
        ) {
            if ($attrs['taxonomy'] !== 'none' && count($attrs['terms']) > 0) {
                $args['tax_query'] = [
                    'taxonomy' => $attrs['taxonomy'],
                    'field' => 'term_id',
                    'terms' => $attrs['terms'],
                ];
            }
        }

        $query = new WP_Query($args);

        if ($query->have_posts()) {
            $this->ids = $query->posts;
        }
    }

    /**
     * Get the WordPress query
     *
     * @param array $attrs Block attributes
     *
     * @return WP_Query Initial data, based on search params
     */
    private function getQuery(array $attrs): WP_Query
    {
        $args = [
            'post__in' => $this->ids,
            'order' => array_key_exists('order', $attrs) ?
                $attrs['order'] :
                'DESC',
            'orderby' => array_key_exists('orderBy', $attrs) ?
                $attrs['orderBy'] :
                'date',
            'paged' => 1,
            'posts_per_page' => array_key_exists('perPage', $attrs) ?
                $attrs['perPage'] :
                12,
        ];

        if (array_key_exists('addPagination', $attrs)) {
            $urlFriendlyPageFilterName = $this->getUrlFriendlyName(
                $this->getPageFilterName()
            ); // Get the page filter name

            foreach ($this->searchParams as $key => $searchParam) {
                if ($searchParam[0] === $urlFriendlyPageFilterName) {
                    $args['paged'] = $searchParam[1][0]; // Set the current page
                    unset($this->searchParams[$key]); // Remove the page search param
                }
            }
        }

        $taxQuery = $this->getTaxonomyQuery();

        if (count($taxQuery) > 0) {
            $args['tax_query'] = $taxQuery;

            if (count($taxQuery) > 1) {
                $args['tax_query']['relation'] = 'AND';
            }
        }

        $query = new WP_Query($args);

        return $query;
    }

    /**
     * Get the initial post data
     *
     * @param WP_Query $query WordPress query
     *
     * @return array Initial data, based on search params
     */
    private function getData(WP_Query $query): array
    {
        return $query->have_posts() ?
            array_map(function (object $post) {
                $helpers = new Helpers();

                return $helpers->filterPost($post);
            }, $query->posts) :
            [];
    }

    /**
     * Get the search params
     *
     * @param string $searchParams Search params string
     *
     * @return array Search params
     */
    private function getSearchParams(string $searchParams): array
    {
        // 'example-param=value,value' => [['example param', ['value', 'value']]
        return array_map(function ($searchParam) {
            $explodedSearchParam = explode('=', urldecode($searchParam));

            return [
                str_replace('-', ' ', $explodedSearchParam[0]),
                explode(',', $explodedSearchParam[1]),
            ];
        }, explode('&', $searchParams));
    }

    /**
     * Get the taxonomy query
     *
     * @return array
     */
    private function getTaxonomyQuery($taxonomySlug = '', $termSlug = ''): array
    {
        $taxQuery = [];
        $taxonomiesInQuery = [];

        foreach ($this->searchParams as $searchParam) {
            $taxonomy = $searchParam[0];

            if (taxonomy_exists($taxonomy)) {
                array_push($taxonomiesInQuery, $taxonomy);

                $terms = (
                    ($taxonomy === $taxonomySlug) &&
                    (!in_array($termSlug, $searchParam[1]))
                ) ?
                    array_merge($searchParam[1], [$termSlug]) :
                    $searchParam[1];

                $taxQuery[] = [
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $terms,
                ];
            }
        }

        if (
            !in_array($taxonomySlug, $taxonomiesInQuery) &&
            $taxonomySlug !== '' &&
            $termSlug !== ''
        ) {
            $taxQuery[] = [
                'taxonomy' => $taxonomySlug,
                'field' => 'slug',
                'terms' => $termSlug,
            ];
        }

        return $taxQuery;
    }

    /**
     * Get the initial filter data
     *
     * @param array $taxonomies Array of taxonomy slugs
     *
     * @return array Array of filter objects
     */
    private function getFilterData(array $filterBlock): array
    {
        if (array_key_exists('filterSlug', $filterBlock['attrs'])) {
            $taxonomySlug = $filterBlock['attrs']['filterSlug'];
            $taxonomy = get_taxonomy($taxonomySlug);

            if ($taxonomy) {
                $filterBlock['attrs']['initialData'] = [
                    'items' => $this->getTaxonomyTerms($taxonomySlug),
                    'name' => $taxonomy->labels->name,
                    'restBase' => $taxonomy->rest_base,
                    'urlFriendlyName' => $this->getUrlFriendlyName(
                        $taxonomy->name
                    ),
                ];
            }
        }

        return $filterBlock;
    }

    /**
     * Get the taxonomy terms
     *
     * @param string $taxonomySlug Taxonomy slug
     *
     * @return array Array of term objects
     */
    private function getTaxonomyTerms(string $taxonomySlug): array
    {
        $query = new WP_Term_Query(
            [
                'taxonomy' => $taxonomySlug,
                'fields' => 'all',
                'exclude' => 1,
            ]
        );
        $terms = $query->get_terms();
        $termObjects = [];

        if (is_array($terms) && count($terms) > 0) {
            foreach ($terms as $term) {
                $args = [
                    'post_type' => 'any',
                    'fields' => 'ids',
                    'post__in' => $this->ids,
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                ];

                $taxQuery = $this->getTaxonomyQuery(
                    $taxonomySlug,
                    $term->slug
                );

                if (count($taxQuery) > 0) {
                    $args['tax_query'] = $taxQuery;

                    if (count($taxQuery) > 1) {
                        $args['tax_query']['relation'] = 'AND';
                    }
                }

                $termQuery = new WP_Query($args);

                if ($termQuery->have_posts()) {
                    $termObject = [
                        'id' => $term->term_id,
                        'slug' => $term->slug,
                        'name' => $term->name,
                        'count' => $termQuery->found_posts
                    ];

                    array_push($termObjects, $termObject);
                }
            }
        }

        return $termObjects;
    }

    /**
     * Get the page filter name
     *
     * @return string Page filter name
     */
    private function getPageFilterName(): string
    {
        return (function_exists('get_field') &&
            get_field('page_filter_name', 'options')
        ) ?
            get_field('page_filter_name', 'options') :
            'page';
    }

    /**
     * Get the url friendly name
     *
     * @param string $name Name
     *
     * @return string Url friendly name
     */
    private function getUrlFriendlyName(string $name): string
    {
        return strtolower(str_replace(' ', '-', $name));
    }
}

<?php

namespace WordPressApiPopulate\Components;

use WordPressApiPopulate\Helpers;

class Editorial
{
    /**
     * Populate editorial
     *
     * @param array $block Editorial block
     *
     * @param array Populated editorial block
     */
    public function populateEditorial(array $block): array {
        if (array_key_exists('postId', $block['attrs'])) {
            $helpers = new Helpers();

            $post = get_post($block['attrs']['postId']);

            $block['attrs']['post'] = $helpers->filterPost($post);
        }

        return $block;
    }
}

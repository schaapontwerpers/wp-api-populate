<?php

namespace WordPressApiPopulate;

class Helpers
{
    /**
     * Filter a post object
     *
     * @param object $post Post object
     *
     * @return array Filtered post object
     */
    public function filterPost(object $post): array
    {
        $postId = $post->ID;

        return $post ? [
            'acf' => function_exists('get_fields') ? get_fields($postId) : [],
            'id' => $postId,
            'link' => get_permalink($postId),
            'type' => $post->post_type,
            'taxonomies' => $this->getPostTerms($postId),
            'title' => [
                'rendered' => $post->post_title,
            ],
        ] : [];
    }

    /**
     * Get the post terms from all taxonomies of a single post
     */
    private function getPostTerms(int $postId): array
    {
        $taxonomies = [];
        $postTaxonomies = get_post_taxonomies($postId);

        foreach ($postTaxonomies as $postTaxonomy) {
            $terms = wp_get_post_terms(
                $postId,
                $postTaxonomy,
                [
                    'fields' => 'all',
                ],
            );

            $taxonomies[$postTaxonomy] = $terms;
        }

        return $taxonomies;
    }
}
